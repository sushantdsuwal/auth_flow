import 'package:flutter/material.dart';
import 'package:auth_flow/ui/screens/LoginScreen.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  var themeData = ThemeData(brightness: Brightness.dark);

  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Login Screen',
      theme: themeData,
      home: LoginScreen(),
    );
  }
}

// class LoginScreen extends StatefulWidget {
//   @override
//   _LoginScreenState createState() => _LoginScreenState();
// }

// class _LoginScreenState extends State<LoginScreen>
//     with SingleTickerProviderStateMixin {
//   AnimationController _iconAnimationController;
//   Animation<double> _iconAnimation;

//   @override
//   void initState() {
//     super.initState();
//     _iconAnimationController = new AnimationController(
//         vsync: this, duration: new Duration(milliseconds: 500));

//     _iconAnimation = new CurvedAnimation(
//         parent: _iconAnimationController, curve: Curves.easeOut);

//     _iconAnimation.addListener(() => this.setState(() {}));
//     _iconAnimationController.forward();
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       body: Stack(
//         fit: StackFit.expand,
//         children: <Widget>[
//           new Image(
//             image: new AssetImage("assets/images/girl.jpeg"),
//             fit: BoxFit.cover,
//             color: Colors.black54,
//             colorBlendMode: BlendMode.darken,
//           ),
//           new Column(
//             mainAxisAlignment: MainAxisAlignment.center,
//             children: <Widget>[
//               _logoWedget(),
//               _formWedget(),
//               _buttonWedget(),
//             ],
//           )
//         ],
//       ),
//     );
//   }

//   _buttonWedget() {
//     return new RaisedButton(
//       child: Text("SIGN IN"),
//       onPressed: () => AlertDialog,
//     );
//   }

//   Widget _logoWedget() {
//     return new FlutterLogo(
//       size: _iconAnimation.value * 100,
//     );
//   }

//   Widget _formWedget() {
//     return new Form(
//         child: new Container(
//       padding: EdgeInsets.all(10.0),
//       child: new Column(
//         crossAxisAlignment: CrossAxisAlignment.center,
//         children: <Widget>[
//           new TextFormField(
//             decoration: InputDecoration(labelText: "Email Address"),
//             keyboardType: TextInputType.emailAddress,
//           ),
//           new TextFormField(
//             decoration: InputDecoration(labelText: "Password"),
//             keyboardType: TextInputType.text,
//             obscureText: true,
//           )
//         ],
//       ),
//     ));
//   }
// }
